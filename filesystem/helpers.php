<?php

/**
 * 跳转提醒方法
 * @param $msg 提醒的内容
 * @param $url 跳转的地址
 * @return 无返回值
 */
function Notice($msg = '', $url = '')
{
    //为了防止script标签提醒alert会乱码，所以加一个编码设置
    @header("Content-Type:text/html;charset=utf-8");

    //获取提示的文本信息
    $msg = empty($msg) ? '未知消息' : trim($msg);

    //url 如果地址为空就返回上一个界面，如果地址不为空就跳转到指定的界面
    if (empty($url)) {
        //跳转上一个界面
        echo "<script>alert('$msg');history.go(-1);</script>";
    } else {
        //跳转到指定页面
        echo "<script>alert('$msg');location.href='$url';</script>";
    }
}


/**
 * 格式化字节大小
 * @param number $size   字节数
 * @param string $delimiter 数字和单位分隔符
 * @return string      格式化后的带单位的大小
 * @author 
 */
function format_bytes($size, $delimiter = '')
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $size >= 1024 && $i < 6; $i++) $size /= 1024;
    return round($size, 2) . $delimiter . $units[$i];
}


/**
 * 递归强制删除
 */
function DeleteDir($path = '')
{
    //要先进入到这个文件夹中
    $list = scandir($path);

    //打开目录后循环遍历
    foreach ($list as $item) {
        if ($item == "." || $item == "..") {
            continue;
        }

        //拼接前半段路径，得到一个完整路径
        $current = $path . '/' . $item;

        //判断是否是文件 如果是文件就直接删除
        if (is_file($current)) {
            @unlink($current);
        } else if (is_dir($current)) {
            //判断是否是文件夹，如果是文件夹就要递归再次循环遍历
            //调用自己本身
            DeleteDir($current);
        }
    }

    //删除为空的文件夹，删除成功返回true
    return @rmdir($path);
}

/**
 * 递归批量删除
 */
function BatchDelete($pathArry)
{
    foreach ($pathArry as $item) {
        if ($item == "." || $item == "..") {
            continue;
        }

        //判断是否是文件 如果是文件就直接删除
        if (is_file($item)) {
            @unlink($item);
        } else if (is_dir($item)) {
            //判断是否是文件夹，如果是文件夹就要递归再次循环遍历
            //调用DeleteDir()
            DeleteDir($item);
        }
        // 判断当前文件是属于什么类型（文件夹还是文件）
    }
    return true;
}



/**
 * 单张上传
 * @param $name 表单input file name的名字
 * @param $path 存放路径
 */
function Upload($name = 'file', $path = '', $type = ['jpg', 'jpeg', 'png', 'gif', 'webp'])
{
    //封装一个结果信息
    $success = [
        'result' => true, //成功还是失败的状态
        'msg' => '', //提醒的内容
        'data' => '' //返回的数据
    ];

    //先获取到上传的文件 是一个数组结构
    $file = @isset($_FILES[$name]) ? $_FILES[$name] : [];

    //判断数组是否为空
    if (empty($file)) {
        $success['result'] = false;
        $success['msg'] = '没有文件上传';
        return $success;
    }

    //判断一下文件是否上传成功是否有错误信息
    $error = $file['error'];
    if ($error > 0) {
        switch ($error) {
            case 1:
                $success['result'] = false;
                $success['msg'] = '超出了服务器上传限制大小';
                break;
            case 2:
                $success['result'] = false;
                $success['msg'] = '超出了表单上传的限制大小';
                break;
            case 3:
                $success['result'] = false;
                $success['msg'] = '网络中断';
                break;
            case 4:
                $success['result'] = false;
                $success['msg'] = '无文件上传';
                break;
            default:
                $success['result'] = false;
                $success['msg'] = '未知错误';
                break;
        }

        return $success;
    }

    //上传文件
    //弄一个唯一的名字
    $filename = date("YmdHis") . "_" . $file['name'];

    //判断是否是规定的类型
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    //判断是否是允许上传的类型
    if (!in_array($ext, $type)) {
        $success['result'] = false;
        $success['msg'] = '非法类型上传';
        return $success;
    }

    //判断存放的目录是否存在，如果不存在就自动创建它
    if (!is_dir($path)) {
        //帮他创建
        $res = mkdir($path, 0777, true);

        //创建失败的时候提醒
        if (!$res) {
            $success['result'] = false;
            $success['msg'] = '创建文件夹失败';
            return $success;
        }
    }

    //将上传的文件完整路径拼接一下
    //第二个参数代表的意思是 如果给了 就删除第二个给的参数的内容
    // 不管你加没加 / 我全部给你们清空 自己加
    $path = trim($path, "/");

    //覆盖一下
    $filename = $path . '/' . $filename;

    //兼容unix
    if (PHP_OS == "Darwin") {
        $filename = "/" . $filename;
    }

    //上传图片
    //判断临时文件是否是安全上传的
    if (is_uploaded_file($file['tmp_name'])) {
        //移动
        $res = move_uploaded_file($file['tmp_name'], $filename);

        if ($res) {
            $success['result'] = true;
            $success['msg'] = '文件上传成功';
            $success['data'] = $filename;
            return $success;
        } else {
            $success['result'] = false;
            $success['msg'] = '文件上传失败';
            return $success;
        }
    } else {
        //不安全就提醒
        $success['result'] = false;
        $success['msg'] = '非法文件';
        return $success;
    }
}

/**
 * 多张图上传
 */

