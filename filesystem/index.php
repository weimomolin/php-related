<?php

// 引入函数库
include_once('helpers.php');

// 解决乱码的问题，先写一个头信息 设置为utf-8编码
@header("Content-Type:text/html;charset=utf-8");

// 接收参数 先判断是否有参数过来
// 如果变量存在就获取，如果变量不存在就默认为空
$path = isset($_GET['path']) ? trim($_GET['path']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';


// 如果为空就读取默认的 demo目录
// 如果为空 其实指的就是首次进来
if (empty($path)) {
    // 定义一个路径
    // $path = '\Users\dancefunk\Sites\localhost\filesystem';
    // 赋值给全局变量
    $path = str_replace("\\", "/", dirname(__FILE__)) . "/demo";
}

// 新建两个变量将文件和文件夹分开存放
$dirs = [];
$files = [];

// 读取路径中的文件列表
$arr = scandir($path);

foreach ($arr as $item) {
    if ($item == "." || $item == "..") {
        continue;
    }

    //要给文件拼接一个完整的路径
    $current = $path . "/" . $item;

    //判断是否是文件还是文件夹
    if (is_dir($current)) {
        //往一个一维数组里面追加一个数组 变成二维数组
        $dirs[] = [
            'name' => $item, //文件名
            'type' => '文件夹', //类型
            'size' => format_bytes(filesize($current)), //文件大小
            'time' => date("Y-m-d H:i", filectime($current)), //创建时间
        ];
    }

    //判断是否是文件
    if (is_file($current)) {
        $files[] = [
            'name' => $item, //文件名
            'type' => pathinfo($current, PATHINFO_EXTENSION), //后缀类型
            'size' => format_bytes(filesize($current)), //文件大小
            'time' => date("Y-m-d H:i", filectime($current)), //创建时间
        ];
    }
}
// ----- 统计文件大小 -----
function GetSize($path, $size = 0)
{
    //要先进入到这个文件夹中
    $list = scandir($path);

    //打开目录后循环遍历
    foreach ($list as $item) {
        if ($item == "." || $item == "..") {
            continue;
        }

        //拼接前半段路径，得到一个完整路径
        $current = $path . '/' . $item;

        if (is_dir($current)) {
            //判断是否是文件夹，如果是文件夹就要递归再次循环遍历
            //调用自己本身
            $size += GetSize($current, $size);
        } else if (is_file($current)) {
            //判断是否是文件
            $size += filesize($current);
        }
    }
    return $size;
}



// ----- 文件夹 -----
// 重命名文件夹
if ($action == "EditDir") {
    //获取新和旧的名字
    $old = isset($_GET['old']) ? trim($_GET['old']) : '';
    $dirname = isset($_GET['dirname']) ? trim($_GET['dirname']) : '';

    if (empty($old)) {
        Notice('旧的文件夹名称不能为空');
        exit;
    }

    if (empty($dirname)) {
        Notice('新的文件夹名称不能为空');
        exit;
    }

    //组装新的名字
    $dirname = $path . '/' . $dirname;

    //判断重命名的文件夹是否存在
    if (is_dir($dirname)) {
        Notice("重命名的文件夹已存在，请重新输入");
        exit;
    }

    //最后创建 rename(旧名字，新名字) 成功修改就返回true
    $res = rename($old, $dirname);

    if ($res) {
        Notice('文件夹重命名成功', "index.php?path=$path");
        exit;
    } else {
        Notice('文件夹重命名失败');
        exit;
    }
}

//删除文件夹
if ($action == "DelDirOne") {
    //先获取到删除文件夹是否存在
    $deldir = isset($_GET['deldir']) ? trim($_GET['deldir']) : '';

    if (empty($deldir)) {
        Notice('删除路径不能为空');
        exit;
    }

    //是否存在
    if (!is_dir($deldir)) {
        Notice('删除路径不存在');
        exit;
    }

    //删除 删除的不是空文件夹 那么rmdir 会报错 非致命错误 warning
    $res = @rmdir($deldir);

    if ($res) {
        Notice('删除成功', "index.php?path=$path");
        exit;
    } else {
        Notice('删除失败');
        exit;
    }
}

// 强制删除
if ($action == "DelDirAll") {
    //先获取到删除文件夹是否存在
    $deldir = isset($_GET['deldir']) ? trim($_GET['deldir']) : '';

    if (empty($deldir)) {
        Notice('删除路径不能为空');
        exit;
    }

    //是否存在
    if (!is_dir($deldir)) {
        Notice('删除路径不存在');
        exit;
    }

    //删除 删除的不是空文件夹 那么rmdir 会报错 非致命错误 warning
    $res = DeleteDir($deldir);

    if ($res) {
        Notice('强制删除成功', "index.php?path=$path");
        exit;
    } else {
        Notice('强制删除失败');
        exit;
    }
}


// ----- 文件 -----
// 重命名文件
if ($action == "EditFile") {
    //获取新和旧的名字
    $old = isset($_GET['old']) ? trim($_GET['old']) : '';
    $dirname = isset($_GET['dirname']) ? trim($_GET['dirname']) : '';

    if (empty($old)) {
        Notice('旧的文件名称不能为空');
        exit;
    }

    if (empty($dirname)) {
        Notice('新的文件名称不能为空');
        exit;
    }

    //组装新的名字
    $dirname = $path . '/' . $dirname . '.' . pathinfo($old, PATHINFO_EXTENSION);

    //判断重命名的文件夹是否存在
    if (is_file($dirname)) {
        Notice("重命名的文件已存在，请重新输入");
        exit;
    }

    //最后创建 rename(旧名字，新名字) 成功修改就返回true
    $res = rename($old, $dirname);

    if ($res) {
        Notice('文件重命名成功', "index.php?path=$path");
        exit;
    } else {
        Notice('文件重命名失败');
        exit;
    }
}

// 删除文件
if ($action == "DelFile") {
    $name = isset($_GET['name']) ? trim($_GET['name']) : '';
    unlink($name);
    Notice('删除成功', "index.php?path=$path");
    exit;
}



// ----- 底部功能模块 -----
//批量删除
if ($_POST) {
    $arry = $_POST['list'];

    $state = BatchDelete($arry);
    if ($state) {
        Notice('删除成功');
        exit;
    } else {
        Notice('删除失败');
        exit;
    }
}

// 创建文件
if ($action == "AddFile") {
    $filename = isset($_GET['filename']) ? $_GET['filename'] : '';
    $fileType = isset($_GET['fileType']) ? trim($_GET['fileType']) : '';

    //文件名称为空，做提醒
    if (empty($filename)) {
        Notice('文件名称为空，请重新填写');
        exit;
    }
    if (empty($fileType)) {
        Notice('文件类型为空，请重新填写');
        exit;
    }
    //组装一个新文件的路径
    $filename = $path . '/' . $filename . '.' .  $fileType;

    //判断是否存在
    if (is_file($filename)) {
        Notice('文件已存在');
        exit;
    }

    //最终创建
    $res = file_put_contents("$filename", "");

    if (!$res) {
        Notice('文件创建成功', "index.php?path=$path");
        exit;
    }
}

// 创建文件夹
if ($action == "AddDir") {
    $dirname = isset($_GET['dirname']) ? $_GET['dirname'] : '';

    //文件夹名称为空，做提醒
    if (empty($dirname)) {
        Notice('文件夹名称为空，请重新填写');
        exit;
    }

    //组装一个新文件夹的路径
    $dirname = $path . '/' . $dirname;

    //判断是否存在
    if (is_dir($dirname)) {
        Notice('文件夹已存在');
        exit;
    }

    //最终创建
    $res = mkdir($dirname, 0777, true);

    if ($res) {
        Notice('文件夹创建成功', "index.php?path=$path");
        exit;
    } else {
        Notice('文件夹创建失败');
        exit;
    }
}

//接收图片
if ($_FILES) {
    //判断是否有图片上传
    if ($_FILES['avatar']['error'] == 0 && $_FILES['avatar']['size'] > 0) {
        $success = Upload('avatar', $path);
        if ($success['result']) {
            Notice($success['msg'], "index.php?path=$path");
            exit;
        } else {
            //上传失败
            Notice($success['msg']);
            exit;
        }
    }
    
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>文件操作系统</title>
    <style>
        table {
            width: 1200px;
            margin: 0 auto;
            margin-top: 80px;
            text-align: center;
            font-size: 1.4em;
        }

        td {
            height: 45px;
            line-height: 45px;
            font-size: 1.2em;
            transition: all linear .2s;
        }

        td:hover {
            background: yellow;
        }

        input[type='checkbox'] {
            width: 1.4em;
            height: 1.4em;
            cursor: pointer;
        }

        #PreviewBox {
            width: 400px;
            margin: 0 auto;
            margin-top: 20px;
            border: 5px dotted #6c6c6c;
            display: none;
        }

        #PreviewBox img {
            width: 100%;
        }
    </style>
</head>

<body>
    <form method="post" enctype="multipart/form-data">
        <table border="1px" cellpadding="0px" cellspacing="0px">
            <tr>
                <td style="width:50px;"><input type="checkbox" id="toggle" /></td>
                <td>文件名称</td>
                <td>文件类型</td>
                <td>文件大小</td>
                <td>创建时间</td>
                <td>操作</td>
            </tr>

            <!-- 文件夹 -->
            <?php foreach ($dirs as $item) { ?>
                <tr>
                    <td><input type="checkbox" class="list" name="list[]" value="<?php echo $path . '/' . $item['name']; ?>" /></td>
                    <td>
                        <a href="index.php?path=<?php echo $path . '/' . $item['name']; ?>">
                            <?php echo $item['name']; ?>
                        </a>
                    </td>
                    <td><?php echo $item['type']; ?></td>
                    <td>
                        <?php
                        $url = $path . '/' . $item['name'];
                        echo format_bytes(GetSize($url, $size));
                        ?>
                    </td>
                    <td><?php echo $item['time']; ?></td>
                    <td>
                        <a href='javascript:void(0)' data-old="<?php echo $path . '/' . $item['name']; ?>" onclick="EditDir(this)">重命名</a>
                        <a href='javascript:void(0)' data-del="<?php echo $path . '/' . $item['name']; ?>" onclick="DelDir(this)">删除</a>
                        <a href='javascript:void(0)' data-del="<?php echo $path . '/' . $item['name']; ?>" onclick="ForceDelDir(this)">强制删除</a>
                    </td>
                </tr>
            <?php } ?>

            <!-- 文件 -->
            <?php foreach ($files as $item) { ?>
                <tr>
                    <td><input type="checkbox" class="list" name="list[]" value="<?php echo $path . '/' . $item['name']; ?>" /></td>
                    <td><?php echo $item['name']; ?></td>
                    <td><?php echo $item['type']; ?></td>
                    <td><?php echo $item['size']; ?></td>
                    <td><?php echo $item['time']; ?></td>
                    <td>
                        <a href='javascript:void(0)' data-old="<?php echo $path . '/' . $item['name']; ?>" ">编辑</a>
                    <a href='javascript:void(0)' data-old=" <?php echo $path . '/' . $item['name']; ?>" onclick="EditFile(this)">重命名</a>
                        <a href='javascript:void(0)' data-name="<?php echo $path . '/' . $item['name']; ?>" onclick="DelFile(this)">删除</a>
                    </td>
                </tr>
            <?php } ?>

            <tr>
                <td colspan="10">
                    <button type="submit" id="btn">批量删除</button>
                    <a href="index.php">首页</a>
                    <a href="javascript:history.go(-1)">后退</a>
                    <a href="javascript:history.go(1)">前进</a>
                    <a href="javascript:void(0)" id="AddFile">创建文件</a>
                    <a href="javascript:void(0)" id="AddDir">创建文件夹</a>

                    <!-- 单独上传 -->
                    <input type="file" name="avatar" id="avatar" style="display: none;" onchange="preview()" />
                    <a href="javascript:void(0)" onclick="avatar.click()">单独上传图片</a>
                    <button type="submit" id="upload">单独上传图片</button>

                    <!-- 多张传输结构 multiple 多选属性 -->
                    <input type="file" name="avatars[]" multiple id="batchUpload" style="display: none;" onchange="previes()" />
                    <a href="javascript:void(0)" onclick="batchUpload.click()">批量上传图片</a>
                    <button type="submit" id="uploads">批量上传图片</button>
                </td>
            </tr>
        </table>
    </form>
    <!-- 预览图片 -->
    <div id="PreviewBox"></div>
</body>

</html>
<script>
    // 建一个变量，用来装 php的 path的变量 读取的路径变量
    var path = "<?php echo $path; ?>"

    // ----- 文件夹 -----
    // 文件夹重命名
    function EditDir(that) {
        //获取元素自身上的data-old属性
        var old = that.dataset.old
        var dirname = prompt('请输入文件夹的名称')
        //如果新的文件夹名称不为空才往下走
        if (dirname) {
            location.href = `index.php?action=EditDir&path=${path}&old=${old}&dirname=${dirname}`
        }
        //return 之后无执行 结束
        return;
    }

    // 删除文件夹
    function DelDir(that) {
        //获取元素自身上的data-name属性
        var deldir = that.dataset.del

        //确认对话框 如果点击确定就返回true 否则就返回false
        if (confirm("是否确认删除")) {
            //页面跳转
            location.href = `index.php?action=DelDirOne&path=${path}&deldir=${deldir}`
        }
        //return 之后无执行 结束
        return;
    }

    // 强制删除文件夹
    function ForceDelDir(that) {
        //获取元素自身上的data-name属性  
        var deldir = that.dataset.del
        //确认对话框 如果点击确定就返回true 否则就返回false
        if (confirm("是否确认强制删除")) {
            //页面跳转
            location.href = `index.php?action=DelDirAll&path=${path}&deldir=${deldir}`
        }


        //return 之后无执行 结束
        return;
    }


    // ----- 文件 -----

    // 文件重命名
    function EditFile(that) {
        //获取元素自身上的data-old属性
        var old = that.dataset.old
        var dirname = prompt('请输入文件的名称')
        //如果新的文件夹名称不为空才往下走
        if (dirname) {
            location.href = `index.php?action=EditFile&path=${path}&old=${old}&dirname=${dirname}`
        }
        //return 之后无执行 结束
        return;
    }

    // 删除文件
    function DelFile(that) {
        //获取元素自身上的data-name属性
        var name = that.dataset.name

        location.href = `index.php?action=DelFile&path=${path}&name=${name}`
        //return 之后无执行 结束
        return;
    }


    // ----- 底部功能模块 -----
    // 全选反选 批量删除
    //获取元素
    var toggle = document.getElementById('toggle')
    //获取列表
    var list = document.getElementsByClassName('list')
    //获取按钮
    var btn = document.getElementById('btn')

    toggle.onclick = function() {
        //下面的列表设置选中状态
        for (var i = 0; i < list.length; i++) {
            list[i].checked = this.checked
        }
    }

    btn.onclick = function(e) {
        //获取选中的元素个数
        var count = 0

        for (var i = 0; i < list.length; i++) {
            if (list[i].checked) {
                count++
            }
        }

        if (count <= 0) {
            alert('请选择批量删除的选项')
            return false
        }

        if (!confirm('是否确认批量删除')) {
            return false
        }
    }

    // 创建文件夹
    var AddDir = document.getElementById('AddDir')
    AddDir.onclick = function() {
        //弹出输入框 输入的值将作为结果返回回来
        var dirname = prompt('请输入文件夹的名称')

        //如果dirname文件夹名称不为空，才去发送请求
        if (dirname) {
            //页面跳转
            // path 当前所在的路径
            // dirname 新的文件夹名称
            //action 告诉php 我现在要执行什么动作 这个值 自己随便写
            // 用来做判断的
            location.href = `index.php?action=AddDir&path=${path}&dirname=${dirname}`
            return;
        }
    }

    // 创建文件
    var AddFile = document.getElementById('AddFile')
    AddFile.onclick = function() {
        //弹出输入框 输入的值将作为结果返回回来
        var filename = prompt('请输入文件的名称')
        var fileType = prompt('请输入文件的类型')

        //如果dirname文件夹名称不为空，才去发送请求
        if (filename && fileType) {
            //页面跳转
            // path 当前所在的路径
            // dirname 新的文件夹名称
            //action 告诉php 我现在要执行什么动作 这个值 自己随便写
            // 用来做判断的
            location.href = `index.php?action=AddFile&path=${path}&filename=${filename}&fileType=${fileType}`
            return;
        }
    }


    // 图片预览（单张）
    function preview() {
        //获取到图片对象 （单次）
        var avatar = document.getElementById('avatar')

        //判断是否有选择图片
        var filelist = avatar.files ? avatar.files : null;
        if (filelist.length <= 0) {
            alert('请选择图片')
            return false
        }

        // 清空动作
        var PreviewBox = document.getElementById('PreviewBox')
        PreviewBox.innerHTML = ''
        PreviewBox.style.display = 'block'

        //ES6 箭头函数
        for (var i = 0; i < filelist.length; i++) {
            loader(filelist[i])
        }

    }

    function loader(file) {
        //创建一个图片获取器
        var reader = new FileReader()

        //加载图片
        reader.readAsDataURL(file)

        //加载成功事件
        reader.onload = function() {
            var img = new Image()
            img.src = this.result
            document.getElementById('PreviewBox').append(img)
        }
    }

    // 单独上传
    var upload = document.getElementById('upload')

    upload.onclick = function() {
        //获取到图片对象
        var avatar = document.getElementById('avatar')

        //判断是否有选择图片
        var file = avatar.files[0] ? avatar.files[0] : null;

        if (!file) {
            alert('请选择图片')
            return false
        }
    }

    // 图片预览（批量）
    function previes() {
        //获取到图片对象 （批量）
        var batchUpload = document.getElementById('batchUpload')

        //判断是否有选择图片
        var batchFiles = batchUpload.files ? batchUpload.files : null;

        if (batchFiles.length <= 0) {
            alert('请选择图片')
            return false
        }

        // 清空动作
        var PreviewBox = document.getElementById('PreviewBox')
        PreviewBox.innerHTML = ''
        PreviewBox.style.display = 'block'

        for (var i = 0; i < batchFiles.length; i++) {
            console.log(batchFiles[i])
            loader(batchFiles[i])
        }
    }
    // 批量上传
    var uploads = document.getElementById('uploads')

    uploads.onclick = function() {
        //获取到图片对象 （批量）
        var batchUpload = document.getElementById('batchUpload')

        //判断是否有选择图片
        var batchFiles = batchUpload.files ? batchUpload.files : null;

        if (!batchFiles) {
            alert('请选择图片')
            return false
        }
    }
</script>