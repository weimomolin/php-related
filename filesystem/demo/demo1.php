<?php

echo "hello world";

//变量
$str = "hello world";

//输出
echo $str;

// 打印
var_dump($str);

// 双引号中的变量会被解析
echo "$str 123123";
echo '$str 123123';

//引用赋值 浅拷贝
$hello = "hello";
$world = &$hello;

//只要是改变其中任意一方，另一方也会跟这变
$hello = '123123';

var_dump($hello, $world);

//超全局变量
if($_GET)
{
    echo 'get';
    var_dump($_GET);
    var_dump($_GET['username']);
}

if($_POST)
{
    echo 'post';
    var_dump($_POST);
}

if($_FILES)
{
    echo '用户获取用户提交的文件信息';
    var_dump($_FILES);
}

//常量

//操作系统
echo PHP_OS;

// PHP版本
echo PHP_VERSION;

//自定义
define('PI', 3.1415926);

// 致命错误
// PI = 123123;

// 非致命错误
// @ 可以抑制非致命的错误，致命的错误抑制不了
echo @$a;

echo PI;

//魔术常量
//可以输出当前这个php文件的路径 绝对路径
echo "<br />";
echo __FILE__;

//读取某个文件
$res = fopen("./php讲义.html", "r");
var_dump($res);


//逻辑短路
// && 逻辑与短路 逻辑与两个必须同时为真结果才是true真

var_dump(1 > 3 && 1 < 2); //false
var_dump(1 < 3 && 1 < 2); //true
// 0 == false
// 1 == true
$a = 0 && 5; //0
var_dump($a);

$res = 1 > 2 ? "是真的" : "是假的";
echo $res;

$arr = ['a','b','c','d'];

//数组循环
foreach($arr as $key=>$item)
{
    echo "索引下标：$key --- 值是：$item<br />";
}


?>
<form method="post" enctype="multipart/form-data">
    用户名：<input type="text" name="username" /><br />
    密码：<input type="password" name="password" /><br />
    头像：<input type="file" name='avatar' /><br />
    <button>提交</button>
</form>