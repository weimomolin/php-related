<?php

// include('./demo1.php');

//once方法会检测你这个文件是否有被引入过，如果有引入过就不会在重复引入了
// include_once('./demo1.php');

// include('aaa.php');
// require('aaa.php');

// echo 'hello world';


//先获取到当前的时间戳
echo time();
echo "<br />";

//将时间戳转换为标准时间结构
echo date("Y-m-d H:i:s", time());
echo "<br />";

//将标准时间变换时间戳结构
echo strtotime("2023-11-11");
echo "<br />";
echo strtotime("2023-11-11 12:23");
echo "<br />";

echo "hello world";
echo 123;

echo "<br />";
echo false;
echo "<br />";
echo true;

var_dump(true);
var_dump(false);


//数组不能用echo 输出

//索引数组 数字下标就是索引数组
$arr = ['a','b','c'];
var_dump($arr);
var_dump($arr[1]);

//关联数组 字母下标就是关联数组 === JS当中对象
$arr = [
    'username'=>'张三',
    'password'=>123123,
    'sex'=>'男'
];
var_dump($arr);
var_dump($arr['username']);

// 密码要做加密处理
echo 123123;
echo "<br />";
echo md5(123456);
echo "<br />";
echo sha1(123456);
echo "<br />";

// 一般密码加密 会配合一个密码盐
$salt = "fs67tdsf";
$password = "123456";

//密码加密
echo md5($password.$salt);



//读取文件内容 将内容读取到字符串当中
$content = file_get_contents("./demo1.php");
var_dump($content);

//写入 如果没有文件就自动新建 返回写入的长度
// utf-8编码的字符 1个中文字 占3个字符
// FILE_APPEND 代表追加模式
$res = file_put_contents("./demo3.php", "重新写入", FILE_APPEND);

//文件夹
// 创建文件夹   ./abc路径文件名 0777权限 true递归创建
//返回值  创建成功返回true 失败返回false
// $res = mkdir('./abc', 0777, true);

//递归创建
// $res = mkdir('./a/b/c', 0777, true);
// var_dump($res);

//修改 文件夹 文件的意思
// $res = rename('./abc-demo', './abc');
// $res = rename('./demo3-3.php', 'demo3.php');
// var_dump($res);

//读取目录下面的所有文件 返回一个数组
$list = scandir('./abc');

var_dump($list);

//对文件夹结构进行数组遍历
foreach($list as $item)
{
    if($item == "." || $item == "..")
    {
        //跳过
        continue;
    }

    //is_file(完整路径) 判断是否是文件
    // is_dir(完整路径) 判断是否是文件夹

    // ./abc/a/  ./abc/demo3.php

    $path = "./abc/".$item;

    if(is_dir($path))
    {
        echo "$item 是一个文件夹<br />";
    }

    if(is_file($path))
    {
        echo "$item 是一个文件 <br />";
    }
}

$str = "D:/phpstudy_pro/WWW/abc.php";

$res = pathinfo($str); //返回一个数组部分

// $res = pathinfo($str, PATHINFO_FILENAME); //单独拿某一个选项
//dirname 路径
// basename 文件名
// extension 后缀部分
// filename 文件名部分
var_dump($res);

//创建时间
echo date("Y-m-d H:i", filectime('./create.php'));

echo "<br />";

//获取文件大小 字节单位
echo filesize("./create.php");

//打印布尔结果
var_dump(file_exists('./create.php'));

var_dump(isset($a));
var_dump(empty($a)); //undefined == 空

$b = "hello world";

//删除变量
unset($b);

echo @$b;

?>