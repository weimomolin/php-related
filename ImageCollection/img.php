<?php
// 设置php运行最大时间限制
error_reporting(E_ALL);
set_time_limit(0);
ini_set('memory_limit', 1000);

// phpinfo();

// exit;

// 设置编码格式
header("Content-Type:text/html;charset=utf-8");

// 获取网页内容
$url = "https://www.bizhi99.com/c16/";
$str = file_get_contents($url);

// 判断网页内容是否获取
if (empty($str)) {
    echo "网页内容获取失败";
    exit;
}

// 在网页上寻找需要采集内容的唯一值，并作为依据，编写正则效验(先获取分页页数及网址)
// <ul class="pagination"><a  >11<  /a></ul>  找出11数字
$reg = "/<ul\s*class=\"pagination\">.*>\s*(\d+)\s*<.*<\/ul>/";
preg_match($reg, $str, $res);

// 获取总页数
$count = isset($res[1]) ? trim($res[1]) : 0;

// 判断是否获取到总页数
if ($count <= 0) {
    echo "分页信息获取失败/n";
    exit;
}

// 获取每一页的链接地址
// 存放链接
$pages = [];

// 组装每一页的链接
for ($i = 0; $i <= $count; $i++) {
    $pages[] = $url . "$i.html";
}

if (empty($pages)) {
    echo "页面链接为空\n";
    exit;
}

//采集图片链接
$piclist = [];

foreach ($pages as $key => $item) {
    echo "第" . ($key + 1) . "页\n";

    //获取每一页的内容
    $content = file_get_contents($item);

    //<div class="item" data-w="5120" data-h="2880">
    $reg = "/<div\s*class=\"item\".*>.*<\/div>/ims";
    preg_match($reg, $content, $res);

    $content = isset($res[0]) ? trim($res[0]) : '';

    if (empty($content)) {
        echo "列表内容为空\n";
        continue;
    }

    // <a href="/bizhi/13990.html" title="萌宠动物萌可爱宠物古灵精怪卖萌图猫喵星人高清壁纸" target="_blank" class="img" style="background:#856d4e"><img class="lazy" src="https://pic.dmjnb.com/pic/d3edc619c3bc9660657f6b725373ba80?imageMogr2/thumbnail/x380/quality/90!" data-original="https://pic.dmjnb.com/pic/d3edc619c3bc9660657f6b725373ba80?imageMogr2/thumbnail/x380/quality/90!" alt="萌宠动物萌可爱宠物古灵精怪卖萌图猫喵星人高清壁纸" style="display: block;"></a>
    $reg = "/<a\s*class=\"title\"\s*href=\"(.*)\".*>.*<\/a>/imsU";
    preg_match_all($reg, $content, $res);

    $current = isset($res[1]) ? array_filter($res[1]) : [];

    for ($i = 0; $i < count($current); $i++) {
    
        $current[$i] = "https://www.bizhi99.com" . $current[$i];
    
        if (empty($current)) {
            echo "第" . ($key + 1) . "页，图片为空";
            continue;
        }
    
        /* 已经获取到了详情页的链接 $current[$key]，下一步获取详情页图片地址
           <div class="wp-bd startlogin" ondragstart="return false" oncontextmenu="return false">
              <img src="https://pic.dmjnb.com/pic/d995b057a011fdac5bd98c9280eda195" alt="萌宠可爱动物小动物猫喵星人高清壁纸">
           </div>
        */
    
        // 获取页面信息
        $con = file_get_contents($current[$i]);
    
        $reg = "/<div\s*class=\"wp-bd startlogin\".*><img\s*src=\"(.*)\".*><\/div>/imsU";
        preg_match_all($reg, $con, $res);
    
        $imgPath = isset($res[1]) ? $res[1] : "";
         $piclist = array_merge($piclist, $imgPath);
    };
}

var_dump($piclist);
exit;
if(empty($piclist))
{
    echo "采集的图片地址为空\n";
    exit;
}

//我们先将文件存放在一个文件夹中
$path = "./uploads/";

//判断目录是否存在
if(!is_dir($path))
{
    $res = mkdir($path, 0777, true);

    if(!$res)
    {
        echo "创建文件夹失败\n";
        exit;
    }
}

echo "采集图片开始\n";
$success = $error = 0;
foreach($piclist as $key=>$item)
{
    //图片和文件都是一样 都是内容
    $content = file_get_contents($item);

    if(empty($content))
    {
        $error++;
        echo $item." 这张图片内容为空\n";
        continue;
    }

    //组装一个文件的新名字
    $filename = ($key+1).".jpg";
    $filename = $path.$filename;

    //将图像的数据，存放在文件中 返回写入的长度
    $length = file_put_contents($filename, $content);

    if($length <= 0)
    {
        echo $item." 写入失败\n";
        $error++;
        continue;
    }else{
        echo $item."载入第$success张";
    }

    $success++;
}


echo "写入成功 $success 张图片 失败写入：$error 张图片\n";
