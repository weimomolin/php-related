<?php
include_once('config/init.php');
include_once('check.php');

//接收管理员id
$id = isset($_GET['id']) ? trim($_GET['id']) : 0;

//根据id查询管理员是否真实存在
$sql = "SELECT * FROM {$pre_}admin WHERE id = $id";
$admin = find($sql);

//当管理员不存在的时候
if (!$admin) {
  Notice('当前编辑的管理员不存在');
  exit;
}


//接收表单
if ($_POST) {
  // 接收数据
  $username = isset($_POST['username']) ? $_POST['username'] : "";
  $oldpass = isset($_POST['oldpass']) ? $_POST['oldpass'] : "";
  $password = isset($_POST['password']) ? $_POST['password'] : "";

  // 判断新旧密码是否一致
  if ($oldpass == $password) {
    Notice("新密码不能与旧密码一样，请重新填写");
    exit;
  };

  // 判断旧密码是否与数据库中密码相同
  // 利用密码盐组装加密密码
  $salt = $admin['salt'];
  // 将密码与密码盐一起进行加密
  $oldpass = md5($oldpass . $salt);

  // 判断加密后的密码是否与数据库中密码相同
  if ($oldpass != $admin['password']) {
    Notice("旧密码输入错误，请检查");
    exit;
  }

  // 将新密码进行加密
  // 随机生成一个密码盐（8位）
  $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  $len = strlen($str) - 1;
  $salt = "";
  for ($i = 0; $i < 8; $i++) {
    $num = mt_rand(0, $len);
    $salt .= $str[$num];
  }
  // 将密码与密码盐一起进行加密
  $password = md5($password . $salt);

  // 组装数据
  $data = [
    "username" => $username,
    "password" => $password,
    "salt" => $salt
  ];

  //头像先判断是否有上传图片
  if ($_FILES['avatar']['error'] == 0 && $_FILES['avatar']['size'] > 0) {
    $path = 'assets/uploads/';
    $success = Upload('avatar', $path);

    if (!$success['result']) {
      Notice($success['msg']);
      exit;
    }
    //直接将路径放到数据中
    $data['avatar'] = $success['data'];
  }

  //更新
  $res = edit("admin", $data, "id = $id");

  if ($res) {
    //判断是否有上传新头像，如果有就要把就图像删掉
    if (isset($data['avatar'])) {
      //员工头像不为空,而且还得是真实存在，在删除
      if (!empty($admin['avatar']) && is_file("./" . $admin['avatar'])) {
        @unlink('./' . $admin['avatar']);
      }
    }

    Notice("编辑管理员成功", "adminList.php");
    exit;
  } else {
    Notice("编辑管理员失败");
    exit;
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- 引入公共样式 -->
  <?php include_once('meta.php'); ?>
  <style>
    .preview {
      width: 170px;
      height: 170px;
      overflow: hidden;
      margin-top: 5px;
    }

    .preview img {
      width: 100%;
    }
  </style>
</head>

<body>
  <!-- 引入头部 -->
  <?php include_once('header.php'); ?>

  <!-- 引入菜单 -->
  <?php include_once('menu.php'); ?>

  <div class="content">
    <div class="header">
      <h1 class="page-title">编辑管理员</h1>
    </div>
    <ul class="breadcrumb">
      <li><a href="index.php">Home</a> <span class="divider">/</span></li>
      <li class="active">编辑管理员</li>
    </ul>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="btn-toolbar">
          <button class="btn btn-primary" onClick="location='adminList.php'"><i class="icon-list"></i> 返回管理员列表</button>
        </div>

        <div class="well">
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane active in" id="home">
              <form method="post" enctype="multipart/form-data">
                <label>管理员名称</label>
                <input type="text" name="username" placeholder="请输入管理员名称" required class="input-xxlarge" value="<?php echo $admin['username']; ?>" />

                <label>管理员旧密码</label>
                <input type="password" name="oldpass" placeholder="请输入旧密码" required class="input-xxlarge" " />

                <label>管理员新密码</label>
                <input type="password" name="password" placeholder="请输入新密码" required class="input-xxlarge" />

                <label>管理员头像</label>
                <input type="file" name="avatar" id="avatar" />

                <div class="preview">
                  <?php if (!empty($admin['avatar']) && is_file("./" . $admin['avatar'])) { ?>
                    <img src="./<?php echo $admin['avatar']; ?>" />
                  <?php } else { ?>
                    <img src="./assets/img/170x170.gif" />
                  <?php } ?>
                </div>

                <label></label>
                <input class="btn btn-primary" type="submit" value="提交" />
              </form>
            </div>
          </div>
        </div>

        <footer>
          <hr>
          <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
        </footer>

      </div>
    </div>
  </div>
</body>

</html>
<script>
  //给图片选择绑定一个改变事件
  $("#avatar").change(function() {
    var avatar = $(this)[0].files[0] ? $(this)[0].files[0] : null

    //如果没有选择图片
    if (!avatar) {
      return;
    }

    //创建一个读取器
    var reader = new FileReader()

    //加载文件
    reader.readAsDataURL(avatar)

    //触发一个加载成功事件
    reader.onload = function(e) {
      //获取加载成功后的图片数据
      // console.log(e.target.result)

      //追加元素
      $(".preview").html(`<img src='${e.target.result}' />`)
    }
  })
</script>