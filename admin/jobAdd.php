<?php
include_once('config/init.php');
include_once('check.php');

//先查询出所有的部门数据
$sql = "SELECT * FROM {$pre_}department";
$deplist = all($sql);


//接收表单
if ($_POST) {

  //要根部门id去查询一下，看该岗位是否存在
  $name = isset($_POST['name']) ? trim($_POST['name']) : '';
  $depid = isset($_POST['depid']) ? trim($_POST['depid']) : '';

  //只要有一个为空，就告诉他重新填写
  if (empty($name) || empty($depid)) {
    Notice("职位名称或者所在部门为空，请重新填写");
    exit;
  }

  // 先去判断是否存在该职位
  $sql = "SELECT * FROM {$pre_}job WHERE name = '$name'";
  // 执行sql
  $query = find($sql);
  // 如果返回有数据，说明职位重复
  if ($query) {
    Notice("已存在该职位，请重新填写");
    exit;
  }

  // 职位不重复，组装数据,准备插入数据
  $data = array(
    'name' => $name,
    'depid' => $depid
  );

  // 插入数据
  $link = add("job", $data);

  // 判断是否插入成功，成功返回插入的自增id
  if ($link <= 0) {
    //插入失败
    Notice("职位添加失败");
    exit;
  } else {
    //插入成功
    Notice("职位添加成功", "jobList.php");
    exit;
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- 引入公共样式 -->
  <?php include_once('meta.php'); ?>
</head>

<body>
  <!-- 引入头部 -->
  <?php include_once('header.php'); ?>

  <!-- 引入菜单 -->
  <?php include_once('menu.php'); ?>

  <div class="content">
    <div class="header">
      <h1 class="page-title">添加职位</h1>
    </div>
    <ul class="breadcrumb">
      <li><a href="index.php">Home</a> <span class="divider">/</span></li>
      <li class="active">添加职位</li>
    </ul>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="btn-toolbar">
          <button class="btn btn-primary" onClick="location='PersonList.php'"><i class="icon-list"></i> 返回职位列表</button>
        </div>

        <div class="well">
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane active in" id="home">
              <form method="post">
                <label>职位名称</label>
                <input type="text" name="name" placeholder="请输入职位名称" required class="input-xxlarge" />

                <label>所在部门</label>
                <select name="depid" required class="input-xlarge">
                  <option value="">请选择</option>
                  <?php foreach ($deplist as $item) { ?>
                    <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                  <?php } ?>
                </select>

                <label></label>
                <input class="btn btn-primary" type="submit" value="提交" />
              </form>
            </div>
          </div>
        </div>

        <footer>
          <hr>
          <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
        </footer>

      </div>
    </div>
  </div>
</body>

</html>
<script>
</script>