<?php
//引入系统配置文件
include_once('config/init.php');
include_once('check.php');

//当前页码
$page = isset($_GET['page']) ? trim($_GET['page']) : 1;

//每页显示多少条
$limit = 5;

//中间显示多少个页码数
$size = 5;

//sql查询数据总数
$sql = "SELECT COUNT(id) AS c FROM {$pre_}person ";
$count = find($sql);
$count = isset($count['c']) ? trim($count['c']) : 0;

//调用分页函数
$html = page($page, $count, $limit, $size, 'black2');

//偏移量
$start = ($page - 1) * $limit;

//表查询
$sql = "SELECT * FROM {$pre_}department ORDER BY {$pre_}department.id DESC LIMIT $start,$limit";

//调用函数
$list = all($sql);

// --------- 删除功能 ---------
// 思路：
// 1、需要删除的部门id已经在depIds中（前端传送过来）
// 2、利用部门id，在职位表中查询出部门下有多少个职位（拿到职位id即可）
// 3、通过职位id，在员工表中查询出所有该职位的员工信息
// 4、先删除部门、再删除职位、再删除员工、最后删除员工头像



// 1、需要删除的部门id已经在depIds中（前端传送过来）
// 判断前端是否发起请求
$action = isset($_POST['action']) ? trim($_POST['action']) : '';
// 如果等于delete 就说明是ajax发送过来 要删除数据
if ($action == "delete") {
    // 封装一个返回结果
    $success = [
        'result' => false,
        'msg' => ''
    ];
    // 获取到删除的部门id数据
    $depIds = isset($_POST['depIds']) ? trim($_POST['depIds']) : 0;


    // 2、利用部门id，在职位表中查询出部门下有多少个职位（拿到职位id即可）
    $sql = "SELECT * FROM {$pre_}job WHERE depid IN($depIds)";
    $jobList = all($sql);
    // 将$jobIds数组形式转换成字符串形式（把id抽离出来）
    $jobIds = implode(',', array_column($jobList, "id"));

    // 判断是否该部门下有职位，若没有职位，直接删除该部门即可，返回结果，不需要进行后续操作
    if (empty($jobIds)) {
        // 删除部门
        $where = "id IN($depIds)";
        $affect_dep = delete('department', $where);
        if (!$affect_dep) {
            //删除失败
            $success['result'] = false;
            $success['msg'] = '删除失败';
        }
        $success['result'] = true;
        $success['msg'] = '删除成功';

        // 将php的数据转换为json的类型，然后并返回给前端的ajax
        echo json_encode($success);
        exit;
    }


    // 3、通过职位id，在员工表中查询出所有该职位的员工信息
    $sql = "SELECT * FROM {$pre_}person WHERE jobid IN($jobIds)";
    $personList = all($sql);
    // 单独抽离出所有员工id，方便后续删除员工信息
    $personIds = implode(',', array_column($personList, "id"));
    // 判断是否该职位下面有员工，没有就直接返回删除成功，并结束，不需要执行删除员工头像操作
    if (empty($personList)) {
        // 删除成功
        $success['result'] = true;
        $success['msg'] = '删除成功';
        // 返回结果
        echo json_encode($success);
        exit;
    }

    // 4、先删除部门、再删除职位、再删除员工、最后删除员工头像
    // 删除部门
    $where = "id IN($depIds)";
    $affect_dep = delete('department', $where);
    if (!$affect_dep) {
        //删除失败
        $success['result'] = false;
        $success['msg'] = '删除失败';
    }

    // 删除职位
    $where = "id IN($jobIds)";
    $affect_job = delete('job', $where);

    if (!$affect_job) {
        //删除失败
        $success['result'] = false;
        $success['msg'] = '删除失败';
    }

    // 删除员工
    $where = "id IN($personIds)";
    $affect_per = delete('person', $where);

    if (!$affect_per) {
        //删除失败
        $success['result'] = false;
        $success['msg'] = '删除失败';
    }

    // 删除头像文件
    // 将文件地址从所有员工中抽离出来
    $avatar = array_column($personList, "avatar");
    // 将数组中的空元素去除 去空
    $avatar = array_filter($avatar);
    if (!empty($avatar)) {
        // 循环删除，必须要图片真实存在，再去删除， 不存在
        foreach ($avatar as $file) {
            //is_file 判断文件是否存在，如果存在返回true
            is_file("./" . $file) && @unlink("./" . $file);
        }
    }
    // 删除成功
    $success['result'] = true;
    $success['msg'] = '删除成功';


    // 返回结果
    echo json_encode($success);
    exit;
}




?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- 引入公共样式 -->
    <?php include_once('meta.php'); ?>

    <!-- 分页样式 -->
    <link rel="stylesheet" href="assets/css/page.css" />
</head>

<body>
    <!-- 引入头部 -->
    <?php include_once('header.php'); ?>

    <!-- 引入菜单 -->
    <?php include_once('menu.php'); ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">部门列表</h1>
        </div>
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a> <span class="divider">/</span></li>
            <li class="active">部门列表</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                <div class="btn-toolbar">
                    <button class="btn btn-primary" onClick="location='DepartmentAdd.php'"><i class="icon-plus"></i> 添加部门</button>
                </div>
                <div class="well">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="toggle" /></th>
                                <th>ID</th>
                                <th>部门名称</th>
                                <th style="width: 60px;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) { ?>
                                <tr>
                                    <td><input type="checkbox" name="list" value="<?php echo $item['id']; ?>" /></td>
                                    <td><?php echo $item['id']; ?></td>
                                    <td><?php echo $item['name']; ?></td>
                                    <td>
                                        <a href="DepartmentEdit.php?id=<?php echo $item['id']; ?>"><i class="icon-pencil"></i></a>
                                        <a class="delone" data-depids="<?php echo $item['id']; ?>" href="#myModal" role="button" data-toggle="modal">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="20">
                                    <!-- 点击是一个锚点 href = id属性 -->
                                    <a class="btn btn-success delall" href="#myModal" role="button" data-toggle="modal">
                                        <i class="icon-remove"></i> 批量删除
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php echo $html; ?>

                <!-- 模态框的元素 -->
                <div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">删除提示框</h3>
                    </div>
                    <div class="modal-body">
                        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>是否确认删除?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
                        <button class="btn btn-danger confirm" data-dismiss="modal">确认删除</button>
                    </div>
                </div>

                <footer>
                    <hr>
                    <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
                </footer>
            </div>
        </div>
    </div>
    <script>
        // 复选框全选反选
        $("#toggle").click(function() {
            //获取到元素选中或取消的值
            // $(this).prop('checked')

            //列表循环的  复选框 
            $("input[name=list]").prop("checked", $(this).prop('checked'))
        });

        // 定义一个全局变量，用来存放当前选中的数据id，方便统一单独删除和批量删除功能
        var depIds = [];

        // 单独删除（每一行右边 X 按钮）
        $(".delone").click(function() {
            // 通过获取自定义属性 data-depids 获得当前点击的管理员id

            var id = $(this).data('depids');
            // 将id放入全局变量中
            depIds = [id];

        });

        // 批量删除
        $(".delall").click(function() {
            // 通过获取绑定的value获得当前选中的id值
            // 获取到所有选中的复选框的值
            var list = $("input[name=list]:checked");

            // 判断是否有选中元素
            if (list.length <= 0) {
                alert('未选择删除的元素')
                return false;
            }

            var arr = []

            //循环元素
            list.each(function(index, item) {
                //往数组里面追加元素
                arr.push(item.value)
            })

            //覆盖全局变量
            depIds = arr
        });


        // 给弹出层的确认删除绑定点击事件
        $(".confirm").click(function() {

            //手动关闭模态框
            $("#myModal").modal('hide');

            //将数组转换为字符串
            var str = depIds.join(',')

            //发送ajax请求
            $.ajax({
                type: 'post', //请求类型
                dataType: 'json', //返回的数据类型
                data: {
                    action: 'delete',
                    depIds: str
                },
                success: function(success) {
                    if (success.result) {
                        location.href = "DepartmentList.php";
                    } else {
                        alert(success.msg)
                    }

                    return false;
                },
                error: function(error) {
                    console.log(error)
                }
            })
        });
    </script>
</body>

</html>