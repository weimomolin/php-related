<!DOCTYPE html>
<html lang="en">
<!-- //引入头部文件 -->
<?php
include_once("config/meta.php");
?>

<body>
    <!-- //引入标题文件 -->
    <?php
    include_once("config/header.php");
    ?>

    <!-- //引入菜单文件 -->
    <?php
    include_once("config/menu.php");
    ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">管理员列表</h1>
        </div>
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a> <span class="divider">/</span></li>
            <li class="active">管理员列表</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                <div class="btn-toolbar">
                    <button class="btn btn-primary" onClick="location='add.html'"><i class="icon-plus"></i>发布文章</button>
                </div>
                <div class="well">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>文章标题</th>
                                <th>发布时间</th>
                                <th style="width: 26px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>必须使用HTML5文档类型 Bootstrap使用的某些HTML元素</td>
                                <td>2013-06-21</td>
                                <td>
                                    <a href="add.html"><i class="icon-pencil"></i></a>
                                    <a href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>必须使用HTML5文档类型 Bootstrap使用的某些HTML元素</td>
                                <td>2013-06-21</td>
                                <td>
                                    <a href="add.html"><i class="icon-pencil"></i></a>
                                    <a href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>必须使用HTML5文档类型 Bootstrap使用的某些HTML元素</td>
                                <td>2013-06-21</td>
                                <td>
                                    <a href="add.html"><i class="icon-pencil"></i></a>
                                    <a href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>必须使用HTML5文档类型 Bootstrap使用的某些HTML元素</td>
                                <td>2013-06-21</td>
                                <td>
                                    <a href="add.html"><i class="icon-pencil"></i></a>
                                    <a href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>必须使用HTML5文档类型 Bootstrap使用的某些HTML元素</td>
                                <td>2013-06-21</td>
                                <td>
                                    <a href="add.html"><i class="icon-pencil"></i></a>
                                    <a href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>必须使用HTML5文档类型 Bootstrap使用的某些HTML元素</td>
                                <td>2013-06-21</td>
                                <td>
                                    <a href="add.html"><i class="icon-pencil"></i></a>
                                    <a href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination">
                    <ul>
                        <li><a href="#">Prev</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div>

                <div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Delete Confirmation</h3>
                    </div>
                    <div class="modal-body">
                        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete the user?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-danger" data-dismiss="modal">Delete</button>
                    </div>
                </div>

                <footer>
                    <hr>
                    <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
                </footer>
            </div>
        </div>
    </div>
</body>

</html>