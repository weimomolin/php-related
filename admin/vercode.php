<?php

//开启session一般都是在最上方
session_start();

//定义一个生成随机字符串的方法
function randstr($length = 4)
{
    $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //可以将字符串随机打乱
    $str = str_shuffle($chars);
    //直接用截取
    return substr($str, 0, $length);
}

//定义验证码宽和高
$width = 80;
$height = 35;

//字体文件的路径
$font = "./assets/font/OpenSans.ttf";

//创建一张彩色的图片
$img = imagecreatetruecolor($width, $height);

//背景颜色 颜色范围 0 - 255 mt_rand给一个范围从这个范围中选择一个随机整数出来
$bgr = mt_rand(0, 255);
$bgg = mt_rand(0, 255);
$bgb = mt_rand(0, 255);
$bgcolor = imagecolorallocate($img, $bgr, $bgg, $bgb);

//将颜色绘制到图片上面
imagefilledrectangle($img, 0, 0, $width, $height, $bgcolor);

//生成一个随机的字符串
$str = randstr();

//cookie是存放在客户端浏览器上面
// setcookie('vercode', $str);

//将验证码存放在session中
$_SESSION['vercode'] = $str;

//字符串可以用数组的方式来调用
// var_dump($str);
// var_dump($str[0]);

for($i=0;$i<strlen($str);$i++)
{
    //文字颜色
    $textr = mt_rand(0, 255);
    $textg = mt_rand(0, 255);
    $textb = mt_rand(0, 255);
    $textcolor = imagecolorallocate($img, $textr, $textg, $textb);
    
    //把文字放到图片当中去
    $x = 20*$i;
    $y = 27;
    imagettftext($img, 20, mt_rand(-30,30), $x, $y, $textcolor, realpath($font), $str[$i]);
}

//添加一些点和折线混淆验证码识别
for($i=0;$i<100;$i++)
{
    //随机颜色
    $r = mt_rand(0, 255);
    $g = mt_rand(0, 255);
    $b = mt_rand(0, 255);
    $color = imagecolorallocate($img, $r, $g, $b);

    $x = mt_rand(0, $width);
    $y = mt_rand(0, $height);

    //将点绘制到图片上 绘制单位1像素 1*1
    imagesetpixel($img, $x, $y, $color);
}

for($i=0;$i<5;$i++)
{
    //随机颜色
    $r = mt_rand(0, 255);
    $g = mt_rand(0, 255);
    $b = mt_rand(0, 255);
    $color = imagecolorallocate($img, $r, $g, $b);

    $x1 = mt_rand(0, $width);
    $y1 = mt_rand(0, $height);
    $x2 = mt_rand(0, $width);
    $y2 = mt_rand(0, $height);

    imageline($img, $x1, $y1, $x2, $y2, $color);
}


//输出图片
// header("Content-Type:text/html;chartset=utf-8");
header("Content-Type:image/png");

//输出png图片
imagepng($img);

?>