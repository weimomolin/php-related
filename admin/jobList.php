<?php
// 引入系统配置文件
include_once('config/init.php');
include_once('check.php');

// 当前页码
$page = isset($_GET['page']) ? trim($_GET['page']) : 1;

// 每页显示多少条
$limit = 4;

// 中间显示多少个页码数
$size = 5;

// sql查询数据总数
$sql = "SELECT COUNT(id) AS c FROM {$pre_}person";
$count = find($sql);
$count = isset($count['c']) ? trim($count['c']) : 0;

// 调用分页函数
$html = page($page, $count, $limit, $size, 'black2');

// 偏移量
$start = ($page - 1) * $limit;

// 表查询
$sql = "SELECT job.*,dep.name AS depname FROM {$pre_}job AS job LEFT JOIN {$pre_}department AS dep ON job.depid = dep.id ORDER BY job.id DESC LIMIT $start,$limit";

// 调用函数
$list = all($sql);


// ----- 删除功能 -----
// 判断是否有post过来数据
$action = isset($_POST['action']) ? trim($_POST['action']) : '';

// 如果等于delete 就说明是ajax发送过来 要删除数据
if ($action == "delete") {
    // 封装一个返回结果
    $success = [
        'result' => false,
        'msg' => ''
    ];
    // 获取到删除的id数据
    $jobIds = isset($_POST['jobIds']) ? trim($_POST['jobIds']) : 0;

    // 先查询出要删除的数据
    $sql = "SELECT * FROM {$pre_}job WHERE id IN($jobIds)";
    $dellist = all($sql);

    if (empty($dellist)) {
        $success['result'] = false;
        $success['msg'] = '暂无删除的数据';

        // 将php的数据转换为json的类型，然后并返回给前端的ajax
        echo json_encode($success);
        exit;
    }

    // 先删除职位
    $deljobIds = implode(',', array_column($dellist, "id"));
    $where = "id IN($deljobIds)";
    $affect_job = delete('job', $where);
    if (!$affect_job) {
        //删除失败
        $success['result'] = false;
        $success['msg'] = '删除失败';
    }


    // 利用职位id，在员工表中，找到该职位下的所有员工，并将其id放入新的数组中
    // 查找员工
    $sql = "SELECT * FROM {$pre_}person WHERE jobid IN($jobIds)";

    $delPersonList = all($sql);


    // 先从二维数组中，抽离出指定的字段，放到一个一位数组
    $avatar = array_column($delPersonList, "avatar");
    $delPersonIds = implode(',', array_column($delPersonList, "id"));


    // 将数组中的空元素去除 去空
    $avatar = array_filter($avatar);

    // 先删除员工数据，再删除图片文件
    $where = "id IN($delPersonIds)";
    $affect = delete('person', $where);


    if ($affect) {
        // 删除成功
        // 先判断图片的数组不是空再去删除
        if (!empty($avatar)) {
            // 循环删除，必须要图片真实存在，再去删除， 不存在
            foreach ($avatar as $file) {
                //is_file 判断文件是否存在，如果存在返回true
                is_file("./" . $file) && @unlink("./" . $file);
            }
        }
        // 删除成功
        $success['result'] = true;
        $success['msg'] = '删除成功';
    } else {
        //删除失败
        $success['result'] = false;
        $success['msg'] = '删除失败';
    }

    //返回结果
    echo json_encode($success);
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- 引入公共样式 -->
    <?php include_once('meta.php'); ?>

    <!-- 分页样式 -->
    <link rel="stylesheet" href="assets/css/page.css" />
</head>

<body>
    <!-- 引入头部 -->
    <?php include_once('header.php'); ?>

    <!-- 引入菜单 -->
    <?php include_once('menu.php'); ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">职位列表</h1>
        </div>
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a> <span class="divider">/</span></li>
            <li class="active">职位列表</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                <div class="btn-toolbar">
                    <button class="btn btn-primary" onClick="location='jobAdd.php'"><i class="icon-plus"></i> 添加职位</button>
                </div>
                <div class="well">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="toggle" /></th>
                                <th>ID</th>
                                <th>职位名称</th>
                                <th>所属部门</th>
                                <th style="width: 60px;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) { ?>
                                <tr>
                                    <td><input type="checkbox" name="list" value="<?php echo $item['id']; ?>" /></td>
                                    <td><?php echo $item['id']; ?></td>
                                    <td><?php echo $item['name']; ?></td>
                                    <td><?php echo $item['depname']; ?></td>
                                    <td>
                                        <a href="jobEdit.php?id=<?php echo $item['id']; ?>"><i class="icon-pencil"></i></a>
                                        <a class="delone" data-jobids="<?php echo $item['id']; ?>" href="#myModal" role="button" data-toggle="modal">
                                            <i class="icon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="20">
                                    <!-- 点击是一个锚点 href = id属性 -->
                                    <a class="btn btn-success delall" href="#myModal" role="button" data-toggle="modal">
                                        <i class="icon-remove"></i> 批量删除
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php echo $html; ?>

                <!-- 模态框的元素 -->
                <div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">删除提示框</h3>
                    </div>
                    <div class="modal-body">
                        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>是否确认删除?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">取消</button>
                        <button class="btn btn-danger confirm" data-dismiss="modal">确认删除</button>
                    </div>
                </div>

                <footer>
                    <hr>
                    <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
                </footer>
            </div>
        </div>
    </div>
</body>

</html>
<script>
    // 复选框全选反选
    $("#toggle").click(function() {
        //获取到元素选中或取消的值
        // $(this).prop('checked')

        //列表循环的  复选框 
        $("input[name=list]").prop("checked", $(this).prop('checked'))
    });

    // 定义一个全局变量，用来存放当前选中的数据id，方便统一单独删除和批量删除功能
    var jobIds = [];

    // 单独删除（每一行右边 X 按钮）
    $(".delone").click(function() {
        // 通过获取自定义属性 data-jobIds 获得当前点击的管理员id

        var id = $(this).data('jobids');
        // 将id放入全局变量中
        jobIds = [id];

    });

    // 批量删除
    $(".delall").click(function() {
        // 通过获取绑定的value获得当前选中的id值
        // 获取到所有选中的复选框的值
        var list = $("input[name=list]:checked");

        // 判断是否有选中元素
        if (list.length <= 0) {
            alert('未选择删除的元素')
            return false;
        }

        var arr = []

        //循环元素
        list.each(function(index, item) {
            //往数组里面追加元素
            arr.push(item.value)
        })

        //覆盖全局变量
        jobIds = arr
    });



    // 给弹出层的确认删除绑定点击事件
    $(".confirm").click(function() {

        //手动关闭模态框
        $("#myModal").modal('hide');

        //将数组转换为字符串
        var str = jobIds.join(',')

        //发送ajax请求
        $.ajax({
            type: 'post', //请求类型
            dataType: 'json', //返回的数据类型
            data: {
                action: 'delete',
                jobIds: str
            },
            success: function(success) {
                if (success.result) {
                    location.href = "jobList.php";
                } else {
                    alert(success.msg)
                }

                return false;
            },
            error: function(error) {
                console.log(error)
            }
        })
    });
</script>