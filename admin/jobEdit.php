<?php
include_once('config/init.php');
include_once('check.php');

// 接收职位id
$id = isset($_GET['id']) ? trim($_GET['id']) : 0;

// 根据id查询职位是否真实存在
$sql = "SELECT * FROM {$pre_}job WHERE id = $id";
$job = find($sql);

// 当职位不存在的时候
if (!$job) {
  Notice('当前编辑的职位不存在');
  exit;
}

// 查询当前职位所属部门信息
$sql = "SELECT * FROM {$pre_}department WHERE id = {$job['depid']} ";
$dep = find($sql);

// 查询所有部门
$sql = "SELECT * FROM {$pre_}department";
$depList = all($sql);


//接收表单
if ($_POST) {
  // 接收数据
  $newName = isset($_POST['name']) ? $_POST['name'] : "";
  $depid = isset($_POST['depid']) ? $_POST['depid'] : "";

  // 判断修改后的名称在数据库中是否已经存在
  // 根据职位名称查询职位是否真实存在
  $sql = "SELECT * FROM {$pre_}job WHERE name = '$newName' AND id != $id";
  $job = find($sql);

  // 当职位不存在的时候
  if ($job) {
    Notice('当前职位名称已存在,请重新填写');
    exit;
  }

  // 组装数据
  $data = [
    "name" => $newName,
    "depid" => $depid
  ];

  //更新
  $res = edit("job", $data, "id = $id");

  if ($res) {
    Notice("编辑职位成功", "jobList.php");
    exit;
  } else {
    Notice("编辑职位失败");
    exit;
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- 引入公共样式 -->
  <?php include_once('meta.php'); ?>
  <style>
    .preview {
      width: 170px;
      height: 170px;
      overflow: hidden;
      margin-top: 5px;
    }

    .preview img {
      width: 100%;
    }
  </style>
</head>

<body>
  <!-- 引入头部 -->
  <?php include_once('header.php'); ?>

  <!-- 引入菜单 -->
  <?php include_once('menu.php'); ?>

  <div class="content">
    <div class="header">
      <h1 class="page-title">编辑职位</h1>
    </div>
    <ul class="breadcrumb">
      <li><a href="index.php">Home</a> <span class="divider">/</span></li>
      <li class="active">编辑职位</li>
    </ul>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="btn-toolbar">
          <button class="btn btn-primary" onClick="location='jobList.php'"><i class="icon-list"></i> 返回职位列表</button>
        </div>

        <div class="well">
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane active in" id="home">
              <form method="post" enctype="multipart/form-data">
                <label>职位名称</label>
                <input type="text" name="name" placeholder="请输入职位名称" required class="input-xxlarge" value="<?php echo $job['name']; ?>" />

                <label>所在部门</label>
                <select name="depid" required class="input-xlarge">
                  <option value="">请选择</option>
                  <?php foreach ($depList as $item) { ?>
                    <option <?php echo $job['depid'] == $item['id'] ? "selected" : ""; ?> value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                  <?php } ?>
                </select>

                <label></label>
                <input class="btn btn-primary" type="submit" value="提交" />
              </form>
            </div>
          </div>
        </div>

        <footer>
          <hr>
          <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
        </footer>

      </div>
    </div>
  </div>
</body>

</html>
<script>
  //给图片选择绑定一个改变事件
  $("#avatar").change(function() {
    var avatar = $(this)[0].files[0] ? $(this)[0].files[0] : null

    //如果没有选择图片
    if (!avatar) {
      return;
    }

    //创建一个读取器
    var reader = new FileReader()

    //加载文件
    reader.readAsDataURL(avatar)

    //触发一个加载成功事件
    reader.onload = function(e) {
      //获取加载成功后的图片数据
      // console.log(e.target.result)

      //追加元素
      $(".preview").html(`<img src='${e.target.result}' />`)
    }
  })
</script>