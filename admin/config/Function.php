<?php
// MySql的增删改查操作函数封装文件

// 编写头部信息
@header("Content-Type:text/html;charset=utf-8");


/**
 * 单条数据查询方法
 * @param $sql 要执行的sql语句
 * @return 返回查询的结果
 */

// ------ 查询 ------

// 查询单条数据
function find($sql = "")
{
    // 引入全局变量
    global $link;

    // 执行SQL语句
    $query = mysqli_query($link, $sql);

    // 判断查询是否成功
    if (!$query) {
        echo "【SQL语句】：$sql <br /> 【错误信息】：" . mysqli_error($link);
        exit;
    }

    //返回出查询的结果 返回关联数组
    return mysqli_fetch_assoc($query);
}
