<?php
// 封装Mysql的增删改查

// 编写头部信息
@header("Content-Type:text/html;charset=utf-8");

/* 
    连接数据库
        host        主机地址
        username    用户名
        password    密码
        dbname      默认使用数据库
        port        连接到 MySQL 服务器的端口
        socket      socket 或要使用的已命名 pipe
    */

// 定义全部变量，连接数据库
$link = mysqli_connect("localhost", "root", "root");
// 判断数据库是否连接成功
if (!$link) {
    echo "链接数据库失败!";
    echo "错误码：" . mysqli_connect_errno();
    echo "[错误信息]：" . mysqli_connect_errno();
    exit;
};

// 选择需要连接的数据库
$select = mysqli_select_db($link, "company");
if (!$select) {
    echo "链接数据库失败";
    exit;
}
// 设置默认的客户端字符集
mysqli_set_charset($link, "utf8");

//设置一个全局的表前缀变量
$pre_ = "pre_";

// 引入外部封装好的 增删改查 函数文件
include_once("function.php");
// 引入封装的错误提示函数文件
include_once("helpers.php");
