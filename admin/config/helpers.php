<?php

/**
 * 跳转提醒方法
 * @param $msg 提醒的内容
 * @param $url 跳转的地址
 * @return 无返回值
 */
function Notice($msg = '', $url = '')
{
    //为了防止script标签提醒alert会乱码，所以加一个编码设置
    @header("Content-Type:text/html;charset=utf-8");

    //获取提示的文本信息
    $msg = empty($msg) ? '未知消息' : trim($msg);

    //url 如果地址为空就返回上一个界面，如果地址不为空就跳转到指定的界面
    if (empty($url)) {
        //跳转上一个界面
        echo "<script>alert('$msg');history.go(-1);</script>";
    } else {
        //跳转到指定页面
        echo "<script>alert('$msg');location.href='/php/admin/$url';</script>";
    }
}
?>