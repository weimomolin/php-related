<?php
include_once('config/init.php');
include_once('check.php');

// 判断部门是否接收到
if ($_POST) {
  $name = isset($_POST["name"]) ? $_POST["name"] : "";

  // 判断数据库中是否存在该部门
  $sql = "SELECT * FROM {$pre_}department WHERE name = '$name'";

  $query = find($sql);

  // 返回结果不为空，说明该部门已存在，弹出提示
  if ($query) {
    Notice("该部门已存在，请重新填写");
    exit;
  }

  // 返回结果为空，说明该部门不存在，执行插入语句
  // 构造关联数组
  $data = array(
    'name' => $name
  );

  $link = add("department", $data);

  if ($link > 0) {
    Notice("添加部门成功","DepartmentList.php");
    exit;
  }
};

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- 引入公共样式 -->
  <?php include_once('meta.php'); ?>
</head>

<body>
  <!-- 引入头部 -->
  <?php include_once('header.php'); ?>

  <!-- 引入菜单 -->
  <?php include_once('menu.php'); ?>

  <div class="content">
    <div class="header">
      <h1 class="page-title">添加部门</h1>
    </div>
    <ul class="breadcrumb">
      <li><a href="index.php">Home</a> <span class="divider">/</span></li>
      <li class="active">添加部门</li>
    </ul>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="btn-toolbar">
          <button class="btn btn-primary" onClick="location='DepartmentList.php'"><i class="icon-list"></i> 返回部门列表</button>
        </div>

        <div class="well">
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane active in" id="home">
              <form method="post">
                <label>部门名称</label>
                <input type="text" name="name" placeholder="请输入部门名称" required class="input-xxlarge" value="" />

                <label></label>
                <input class="btn btn-primary" type="submit" value="提交" />
              </form>
            </div>
          </div>
        </div>

        <footer>
          <hr>
          <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
        </footer>

      </div>
    </div>
  </div>
</body>

</html>
<script>

</script>