<?php
include_once('config/init.php');
include_once('check.php');

// 判断数据是否接收到
if ($_POST) {
  // 接收数据
  $username = isset($_POST['username']) ? $_POST['username'] : "";
  $password = isset($_POST['password']) ? $_POST['password'] : "";

  // 判断添加的管理员是否存在
  if (empty($username) || empty($password)) {
    Notice("账号名称或者密码为空，请重新填写");
    exit;
  }

  // 查询数据库中是否存在该管理员信息
  $sql = "SELECT * FROM {$pre_}admin WHERE username = '$username'";

  // 只要是查询出一条就说明重复录入了
  $check = find($sql);

  // 如果不为空，就说明查询到了，查询到就不能录入了
  if (!empty($check)) {
    Notice('该管理员账号已存在，请重新填写');
    exit;
  }

  // 没有重复，则进行密码加密
  // 随机生成一个密码盐（8位）
  $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  $len = strlen($str) - 1;
  $salt = "";
  for ($i = 0; $i < 8; $i++) {
    $num = mt_rand(0, $len);
    $salt .= $str[$num];
  }
  // 将密码与密码盐一起进行加密
  $password = md5($password . $salt);

  // 将要添加的管理员信息插入数据库中
  // 组装数据
  $data = [
    "username" => $username,
    "password" => $password,
    "salt" => $salt
  ];



  // 判断是否上传头像
  if ($_FILES['avatar']['error'] == 0 && $_FILES['avatar']['size'] > 0) {
    $path = 'assets/uploads/';
    $success = Upload('avatar', $path);

    if (!$success['result']) {
      Notice($success['msg']);
      exit;
    }

    //直接将路径放到数据中
    $data['avatar'] = $success['data'];
  }

  // var_dump($data);
  // exit;

  // 将组装好的数据插入到管理员表中
  $res = add("admin",$data);
  if($res)
  {
    Notice("添加管理员成功", "adminList.php");
    exit;
  }else
  {
    Notice("添加管理员失败");
    exit;
  }
};

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- 引入公共样式 -->
  <?php include_once('meta.php'); ?>
  <style>
    .preview {
      width: 170px;
      height: 170px;
      overflow: hidden;
      margin-top: 5px;
    }

    .preview img {
      width: 100%;
    }
  </style>
</head>

<body>
  <!-- 引入头部 -->
  <?php include_once('header.php'); ?>

  <!-- 引入菜单 -->
  <?php include_once('menu.php'); ?>

  <div class="content">
    <div class="header">
      <h1 class="page-title">添加管理员</h1>
    </div>
    <ul class="breadcrumb">
      <li><a href="index.php">Home</a> <span class="divider">/</span></li>
      <li class="active">添加管理员</li>
    </ul>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="btn-toolbar">
          <button class="btn btn-primary" onClick="location='adminList.php'"><i class="icon-list"></i> 返回管理员列表</button>
        </div>

        <div class="well">
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane active in" id="home">
              <form method="post" enctype="multipart/form-data">
                <label>管理员账号</label>
                <input type="text" name="username" placeholder="请输入管理员账号" required class="input-xxlarge" value="" />
                <label>管理员密码</label>
                <input type="password" name="password" placeholder="请输入管理员密码" required class="input-xxlarge" value="" />
                <label>管理员头像</label>
                <input type="file" name="avatar" id="avatar" />
                <div class="preview">
                  <img src="./assets/img/170x170.gif" />
                </div>
                <label></label>
                <input class="btn btn-primary" type="submit" value="提交" />
              </form>
            </div>
          </div>
        </div>

        <footer>
          <hr>
          <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
        </footer>

      </div>
    </div>
  </div>
</body>

</html>
<script>
  // 监听头像上传事件
  $("#avatar").change(function(e) {
    console.log(e.target.files[0]);
    // 获取上传的头像
    var avater = e.target.files[0] ? e.target.files[0] : null;
    // 如果没有选择图片
    if (!avater) {
      return;
    }

    // 图片预览功能
    // 创建一个读取器
    var reader = new FileReader();
    // 加载图片文件
    reader.readAsDataURL(avater);
    // 触发加载成功事件
    reader.onload = function(e) {
      //获取加载成功后的图片数据
      // console.log(e.target.result)

      //追加元素
      $(".preview").html(`<img src='${e.target.result}' />`)
    }
  });
</script>