<?php
//引入系统配置文件
include_once('config/init.php');
include_once('check.php');

//查询员工数据
$sqlPerson = "SELECT * FROM {$pre_}person ORDER BY createtime DESC";

//链表查询
// 员工表.depid = 部门表.id
// 员工表.jobid = 职位表.id
// $sql = "SELECT person.*,dep.name AS depname,job.name AS jobname FROM {$pre_}person AS person LEFT JOIN {$pre_}department AS dep ON person.depid = dep.id LEFT JOIN {$pre_}job AS job ON person.jobid = job.id";

// //调用函数
// $list = all($sql);
// var_dump($list);
// exit;

// 分页操作 （固定每一页显示5条数据，通过计算数据库中的总数据量 / 5 得出一共有多少页 ）
// 获取到总数据长度
$num = count(all($sqlPerson));
// 每一页最多显示多少条数据
$pageSize = 5;
// 一共有多少页 ceil()向上取整
$page = ceil($num / $pageSize);
// 获取到当前界面是多少页
$CurPage = isset($_GET["CurPage"]) ? trim($_GET["CurPage"]) : 1;

if ($CurPage < 1) $$CurPage = 1;
if ($CurPage > $page) $CurPage = $page;

// 计算偏移量
// 数据库查询起点
$stratPage = ($CurPage - 1) * $pageSize;

$sql = "SELECT person.*,dep.name AS depname,job.name AS jobname FROM {$pre_}person AS person LEFT JOIN {$pre_}department AS dep ON person.depid = dep.id LEFT JOIN {$pre_}job AS job ON person.jobid = job.id LIMIT $stratPage , $pageSize";

//调用函数
$list = all($sql);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- 引入公共样式 -->
    <?php include_once('meta.php'); ?>
</head>

<body>
    <!-- 引入头部 -->
    <?php include_once('header.php'); ?>

    <!-- 引入菜单 -->
    <?php include_once('menu.php'); ?>

    <div class="content">
        <div class="header">
            <h1 class="page-title">员工列表</h1>
        </div>
        <ul class="breadcrumb">
            <li><a href="index.php">Home</a> <span class="divider">/</span></li>
            <li class="active">员工列表</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                <div class="btn-toolbar">
                    <button class="btn btn-primary" onClick="location='addPerson.php'"><i class="icon-plus"></i> 添加员工</button>
                </div>
                <div class="well">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="SelectAll" /></th>
                                <th>姓名</th>
                                <th>性别</th>
                                <th>手机号</th>
                                <th>邮箱</th>
                                <th>头像</th>
                                <th>部门</th>
                                <th>职位</th>
                                <th>入职时间</th>
                                <th style="width: 60px;">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list as $item) { ?>
                                <tr>
                                    <td><input type="checkbox" name="list" /></td>
                                    <td><?php echo $item['name']; ?></td>
                                    <?php if ($item['sex'] == '0') { ?>
                                        <td>保密</td>
                                    <?php } else if ($item['sex'] == '1') { ?>
                                        <td>男</td>
                                    <?php } else if ($item['sex'] == '2') { ?>
                                        <td>女</td>
                                    <?php } else { ?>
                                        <td></td>
                                    <?php } ?>
                                    <td><?php echo $item['mobile']; ?></td>
                                    <td><?php echo $item['email']; ?></td>
                                    <?php if (is_file("./assets" . $item['avatar'])) { ?>
                                        <td>
                                            <a style="display: block;width:100px;height:100px" href="<?php echo "." . $item['avatar']; ?>" target="_blank">
                                                <img src="<?php echo "./assets" . $item['avatar']; ?>">
                                            </a>
                                        </td>
                                    <?php } else { ?>
                                        <td>暂无头像</td>
                                    <?php } ?>
                                    <td><?php echo $item['depname']; ?></td>
                                    <td><?php echo $item['jobname']; ?></td>
                                    <td><?php echo date("Y-m-d", $item['createtime']); ?></td>
                                    <td>
                                        <a href="add.html"><i class="icon-pencil"></i></a>
                                        <a href="#myModal" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <a href="javascript:void(0);" id="btn">批量删除</a>
                </div>
                <div class="pagination">
                    <ul>
                        <?php if ($CurPage > 1) { ?>
                            <li><a href="<?php echo "PersonList.php?CurPage=" . ($CurPage - 1) ?>">上一页</a></li>
                        <?php } ?>
                        <?php for ($i = 1; $i <= $page; $i++) { ?>
                            <li><a href="<?php echo "PersonList.php?CurPage=" . $i ?>"><?php echo $i; ?></a></li>
                        <?php } ?>
                        <?php if ($CurPage < $page) { ?>
                            <li><a href="<?php echo "PersonList.php?CurPage=" . ($CurPage + 1) ?>">下一页</a></li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Delete Confirmation</h3>
                    </div>
                    <div class="modal-body">
                        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete the user?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-danger" data-dismiss="modal">Delete</button>
                    </div>
                </div>

                <footer>
                    <hr>
                    <p>&copy; 2017 <a href="#" target="_blank">copyright</a></p>
                </footer>
            </div>
        </div>
    </div>
</body>
<script>
    // ------- 全选功能 -------
    var list = $('input[name=list]');
    $("#SelectAll").click(function() {
        for (i = 0; i < list.length; i++) {
            list[i].checked = this.checked;
        }
    });
    // 批量删除功能
</script>

</html>