<?php
// 封装、继承、多态


// 访问修饰符
// public 公共修饰符
//     父类：内部和外部都能访问
//     继承的子类：内部和外部都能访问

// protected 受保护修饰符
//     父类：内部能访问，外部不能访问
//     继承的子类：内部能访问，外部不能访问

// private 私有修饰符
//     父类：内部能访问，外部不能访问
//     继承的子类：内部不能访问，外部不能访问


// 创建一个类
class Person
{
    // 属性
    public $name;
    public $age;
    public $money;

    // protected 受保护
    protected $pro;

    // private 私有
    private $pri;

    // 静态属性
    static $dom = "父类静态属性";
    // 常量
    const PI = 3.1415926;

    // 构造函数（实例化自动执行）、析构函数（使用完对象后自动执行） 
    // 都是以__开头

    // __construct构造函数（作用：主要用于初始化类的属性）
    public function __construct($name, $age, $money)
    {
        echo "执行构造函数中...";
        // 初始化属性
        $this->name = $name;
        $this->age = $age;
        $this->money = $money;
        $this->pro = "父类受保护";
        $this->pri = "父类私有";

        echo "构造函数执行完成";
    }

    // __destruct析构函数(作用：主要用于使用完对象后，清空复原某些设置)
    public function __destruct()
    {
        echo "执行析构函数中...";
        $this->name = " ";
        echo $this->name;
        echo "析构函数执行完成";
    }

    // 静态方法
    static function sleep()
    {
        return self::$dom;
    }

    // 公开方法
    public function work()
    {
        $this->money += 100;
        echo $this->money;
    }
}
// 实例化父类
$per = new Person("父类", 50, 0);
var_dump($per);

// 外部使用
// 公开的属性
var_dump($per->name);
// 受保护的属性(报错)
// echo $per->pro;
// 私有的属性(报错)
// echo $per->pri;
// 静态属性
// echo $per::$dom;
// 常量
// echo $per::PI;

// 调用静态方法
$per::sleep();

// 调用公开方法
$per->work();

echo "<br>";

// 创建一个子类
class Son extends Person
{
    public $emali = "子类邮箱";

    // 继承父类构造函数
    public function __construct($name, $age, $money,$txt)
    {
        // 手动继承父类的构造函数  可以不用再重复写父类中的构造函数方法体
        parent::__construct($name, $age, $money);

        echo $txt;
    }
}

$son = new Son("子类",10,0,"<br>子类重写父类构造函数（多态）");
var_dump($son);