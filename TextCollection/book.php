<?php

// 设置临时脚本运行时间，0代表无限时间
ini_set('max_execution_time', 0);

// 设置头部，与采集网站编码模式进行统一
header("Content-Type:text/html;charset=utf-8");

// 获取网页界面内容（先去拿去总的小说章节名称）
$str = file_get_contents("https://www.qidian.com/book/1037486330/");

// 判断网页内容是否获取成功
if (empty($str)) {
    echo "网页界面内容获取失败\n";
    exit;
}

// 查看小说章节列表所在的唯一值  <ul class="volume-chapters">
// 根据唯一值编写正则效验   i 不区分大小写   m 视为多行   s 匹配空白字符   U  取消贪婪匹配 
$reg = "/<ul\s*class=\"volume-chapters\">(.*)<\/ul>/imsU";
preg_match_all($reg, $str, $res);
// 判断是否获取到了章节列表
$list = isset($res[0][0]) ? trim($res[0][0]) : '';

// 判断章节目录列表是否获取成功
if (empty($list)) {
    echo "匹配列表失败\n";
    exit;
}


// 通过获取的列表，找到最终需要的href属性中的地址
/*
  <a class="chapter-name" href="//www.qidian.com/chapter/1037486330/759473093/"
  target="_blank" data-eid="qd_G55" data-cid="//www.qidian.com/chapter/1037486330/759473093/"
  alt="远星尘梦记 楔子在线阅读" title="远星尘梦记 楔子 首发时间：2023-07-09 17:30:48 章节字数：3855">
  楔子</a>
*/
// 编写正则效验
$reg = "/<a\s*class=\"chapter-name\"\s*href=\"([^\"]*?)\".*?>(.*?)<\/a>/ims";
preg_match_all($reg, $list, $res);

//判断是否有拿到结果
// array_filter() 可以过滤掉数组中的空元素
$urlist = isset($res[1]) ? array_filter($res[1]) : [];
$titlelist = isset($res[2]) ? array_filter($res[2]) : [];

if (empty($urlist) || empty($titlelist)) {
    echo "链接或者标题为空\n";
    exit;
}

if (count($urlist) != count($titlelist)) {
    echo "匹配的数量不一致\n";
    exit;
}
// var_dump($urlist);
// exit;
echo "开始采集内容\n";

// 定义一个新数组，将路径和标题重构成新的数组，拼接地址 https:$urlist+$titlelist[i];
$result = "";

foreach ($urlist as $key => $item) {
    // 获取标题
    $title = $titlelist[$key];

    // 拼接地址
    $url = "https:" . $item;

    // 获取每一章的界面内容
    $content = file_get_contents($url);

    // 获取到小说具体的内容
    /* <main id="c-760047376" data-type="cjk" 
   class="content mt-1.5em text-s-gray-900 leading-[1.8] 
   relative z-0 r-font-black" data-v-14130e9d=""></main>
*/
    // 根据上面唯一值<main>编写正则效验
    $reg = "/<main.*?>(.*?)<\/main>/ims";
    preg_match($reg, $content, $res);

    $content = isset($res[0]) ? $res[0] : '';
  
    if (empty($content)) {
        echo $title . " 采集内容为空跳过\n";
        continue;
    }

    //替换内容 将<p></p>替换为\r\n
    $content = preg_replace("/<p>|<\/p>/", "\r\n", $content);
    $content = $title."\r\n\r\n".$content."\r\n\r\n";

    //拼接到总变量中
    $result .= $content;

    echo "采集到：$title \n";
}

//将采集的内容保存到本地
if (empty($result)) {
    echo "为采集到任何内容\n";
    exit;
}

//文件名
$filename = date("YmdHi") . ".txt";

//写入内容 返回写入的长度
$length = file_put_contents("./$filename", $result);

if ($length > 0) {
    echo '采集完毕\n';
} else {
    echo '采集为空\n';
}
